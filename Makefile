.DEFAULT_GOAL := help

ifndef CONDA_PREFIX
$(error Conda not active, please install conda and then create an environment with `conda create --name cmip6calibrationdata` and activate it with `conda activate cmip6calibrationdata`))
else
ifeq ($(CONDA_DEFAULT_ENV),base)
$(error Do not install to conda base environment. Source a different conda environment e.g. `conda activate cmip6calibrationdata` or `conda create --name cmip6calibrationdata` and rerun make with the `-B` flag))
endif
endif

PYTHON=$(CONDA_PREFIX)/bin/python
CONDA_ENV_FILE=environment.yml

FILES_TO_FORMAT_PYTHON=scripts/*.py

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT


help:  ## print short description of each target
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)


.PHONY: black
black: $(CONDA_PREFIX)  ## use black to autoformat code
	@status=$$(git status --porcelain); \
	if test ${FORCE} || test "x$${status}" = x; then \
		$(CONDA_PREFIX)/bin/black --exclude _version.py --target-version py37 $(FILES_TO_FORMAT_PYTHON); \
	else \
		echo Not trying any formatting, working directory is dirty... >&2; \
	fi;

.PHONY: zombimon
zombimon:  ## do all zombimon calculations
	bash scripts/run-zombimon.sh

.PHONY: conda-environment
conda-environment: $(CONDA_PREFIX)  ## create conda environment for processing data
$(CONDA_PREFIX): $(CONDA_ENV_FILE)
	$(CONDA_EXE) env update --name $(CONDA_DEFAULT_ENV) -f $(CONDA_ENV_FILE)
	touch $(CONDA_PREFIX)


.PHONY: clean
clean:
	rm -rf $(CONDA_PREFIX)

.PHONY: clean-crunched
clean-crunched:
	rm -r crunched/CMIP6

.PHONY: clean-data
clean-data:
	rm -r data/CMIP6/ascii

.PHONY: clean-database
clean-database:
	rm -r data/CMIP6/database

.PHONY: clean-zombimon
clean-zombimon:
	rm -r data/CMIP6/zombimon

.PHONY: clean-all
clean-all: clean-database  clean-zombimon clean-data clean-crunched

