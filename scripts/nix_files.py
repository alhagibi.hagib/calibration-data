"""
Filter out invalid files from the database

The files still remain in the archive, but they won't be displayed on the website or used in analysis
"""
import click
import pandas as pd

pd.set_option("display.max_colwidth", None)
pd.set_option("display.max_columns", None)
pd.set_option("display.width", None)

files_to_nix = [
    {
        "source_id": "MRI-ESM2-0",
        "grid_label": "gn",
        "variable_id": "tos"
    },
    {
        "source_id": "MRI-ESM2-0",
        "grid_label": "gn",
        "variable_id": "fgco2"
    },
    {
        "source_id": "MRI-ESM2-0",
        "grid_label": "gn",
        "variable_id": "hfds"
    },
]


@click.command()
@click.option("--dry-run/--no-dry-run", default=True)
@click.argument("database", type=click.Path(exists=True, file_okay=True, dir_okay=False, resolve_path=True, readable=True))
def main(dry_run, database):
    df = pd.read_csv(database)

    for filter in files_to_nix:
        click.echo("Filter: {}".format(filter))

        filter_mask = None
        for f in filter:
            if filter_mask is None:
                filter_mask = df[f] == filter[f]
            else:
                filter_mask = filter_mask & (df[f] == filter[f])

        bad_count = filter_mask.sum() if filter_mask is not None else 0
        click.echo("Removing {} entries".format(bad_count))
        if bad_count:
            click.echo(df[filter_mask])

        df = df[~filter_mask]

    if not dry_run:
        click.echo("Writing database")
        df.to_csv(database, index=None)


if __name__ == "__main__":
    main()
