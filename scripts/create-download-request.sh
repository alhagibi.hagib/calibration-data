#!/bin/bash
#
# Process CMIP6 data
#

if [ $# -eq 0 ]
  then
    echo "No arguments supplied. Pass config filename"
    exit
fi

echo "Reading configuration from $1"
export CONFIG=$(cat "$1")

# Load helper funcs
export SCRIPT_DIR=$(dirname "$BASH_SOURCE")
source ${SCRIPT_DIR}/process/funcs.sh

TARGET=$(get_config '.target') || exit 1
DRS=$(get_config '.drs') || exit 1
SOURCE_ID=$(get_config '.source_id' true) || exit 1
VARIANT_LABEL=$(get_config '.variant_label' true) || exit 1
VARIABLE_ID=$(get_config '.variable_id' true) ||exit 1
EXPERIMENT_ID=$(get_config '.experiment_id' true) || exit 1


# Spit out the config
echo "mip_era=CMIP6"

if [ "${SOURCE_ID}" != ".*" ]; then

    echo "source_id=${SOURCE_ID//,/ }"

else

    echo "source_id=all"

fi

if [ "${VARIANT_LABEL}" != ".*" ]; then

    echo "variant_label=${VARIANT_LABEL//,/ }"

else

    echo "variant_label=all"

fi

if [ "${VARIABLE_ID}" != ".*" ]; then

    echo "variable_id=${VARIABLE_ID//,/ }"

else

    echo "variable_id=all"

fi

if [ "${EXPERIMENT_ID}" != ".*" ]; then

    echo "experiment_id=${EXPERIMENT_ID//,/ }"

else

    echo "experiment_id=all"

fi
