from collections import defaultdict
from glob import glob
from os.path import join

import pandas as pd
import tqdm.autonotebook as tqdm
from netcdf_scm.citing import get_citation_tables
from netcdf_scm.io import load_mag_file, pymagicc
from netcdf_scm.retractions import check_depends_on_retracted
from scmdata import ScmRun

# make sure tables don't get cut off
pd.set_option("display.max_colwidth", None)


def short_read(fp):
    out = ScmRun(
        data=[1],
        index=[1],
        columns={
            "model": "not set",
            "scenario": "not set",
            "variable": "not set",
            "unit": "not set",
            "region": "not set",
            "todo": "not set"
        },
    )
    out.metadata = pymagicc.io.read_mag_file_metadata(fp)

    return out


pymagicc.io.MAGICCData = short_read

files_to_check = glob(
    join("data", "CMIP6", "ascii", "mag", "**", "*.MAG"), recursive=True
)
db_cmip6 = [
    load_mag_file(f, drs="CMIP6Output")
    for f in tqdm.tqdm(files_to_check, desc="Loading files")
]

retraction_table = check_depends_on_retracted(
    files_to_check, esgf_query_batch_size=20, nworkers=8
)
retraction_table.to_csv("retraction_table.csv", index=False)


retracted_files = retraction_table.loc[retraction_table["dependency_retracted"], :]

retracted_mag_files = retracted_files["mag_file"].unique()
print("{} MAG files based on retracted data".format(retracted_mag_files.shape[0]))
#print(retracted_mag_files.tolist())
print()

retracted_nc_files = retracted_files["dependency_file"].unique()
print("{} retracted nc files".format(retracted_nc_files.shape[0]))
#print(retracted_nc_files.tolist())
