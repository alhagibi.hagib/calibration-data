#!/bin/env python
"""
Creates the database prefix.

Creates CSV files of all available files
"""
import os
from concurrent.futures import as_completed, ProcessPoolExecutor
from glob import glob
from os.path import join, dirname, exists, relpath, basename

import numpy as np
import click
import numpy.testing as npt
import pandas as pd
from netcdf_scm.iris_cube_wrappers import CMIP6OutputCube, MarbleCMIP5Cube
from openscm_units import unit_registry
from pint.errors import DimensionalityError, UndefinedUnitError
from pymagicc.io import MAGICCData
from tqdm import tqdm


def get_helper(target):
    if target == "cmip5":
        return MarbleCMIP5Cube()
    else:
        return CMIP6OutputCube()


def _clean_cmip_unit_for_pint(cmip_unit):
    if "e-" in cmip_unit:
        return cmip_unit

    return cmip_unit.replace("-", "^-")


def _get_cmip_unit(ids, cmip_units):
    try:
        variable = ids["variable_id"]
    except KeyError:
        variable = ids["variable_name"]

    cmip_var_row = cmip_units.loc[
        cmip_units["variable_id"] == variable, :
    ].drop_duplicates()
    if len(cmip_var_row) == 0:
        return False, variable
    assert cmip_var_row.shape[0] == 1, variable

    cmip_unit = cmip_var_row["units"].iloc[0]
    # unit = _clean_our_unit(ids["unit"])
    unit = ids["unit"]

    cmip_unit_pint_friendly = _clean_cmip_unit_for_pint(cmip_unit)

    error_msg = "Variable: {}. Our unit: {}, CMIP unit: {}, CMIP unit pint friendly: {}".format(
        variable, unit, cmip_unit, cmip_unit_pint_friendly
    )

    try:
        npt.assert_equal(
            unit_registry(unit).to(cmip_unit_pint_friendly).magnitude,
            1,
            err_msg=error_msg,
        )
    except (AssertionError, ValueError, DimensionalityError, UndefinedUnitError) as e:
        return False, "{}. {}".format(error_msg, str(e))

    return True, cmip_unit


def _process_file(filename, helper, release_dir, cmip_units):
    filename_clean = (
        filename.replace("netcdf-scm_", "")
        .replace("NORMED_", "")
        .replace("DEDRIFTED_", "")
        .replace("NORMED30_", "")
        .replace("DEDRIFTED30_", "")
    )
    dirpath = dirname(filename)
    ids = helper.get_load_data_from_identifiers_args_from_filepath(filename_clean)
    # Make root dir relative to the release directory
    ids["root_dir"] = relpath(ids["root_dir"], release_dir)

    if "monthly" in dirpath:
        timeseriestype = "monthly"
    elif "average-year-mid-year" in dirpath:
        timeseriestype = "average-year-mid-year"
    elif "average-year-start-year" in dirpath:
        timeseriestype = "average-year-start-year"
    elif "point-start-year" in dirpath:
        timeseriestype = "point-start-year"
    else:
        raise ValueError("unrecognised timeseriestype")

    ids["timeseriestype"] = timeseriestype
    ids["normalised"] = "NORMED" in filename or "DEDRIFTED" in filename

    # have to open filename to read units...
    tmp = MAGICCData(filename)
    ids["unit"] = tmp.get_unique_meta("unit", no_duplicates=True)
    clean_convert_cmip, unit_cmip = _get_cmip_unit(ids, cmip_units)

    unit_failures = None
    if clean_convert_cmip:
        ids["unit_cmip"] = unit_cmip
    else:
        unit_failures = unit_cmip
        ids["unit_cmip"] = None

    if ids["normalised"]:
        try:
            ids["normalisation_method"] = tmp.metadata["normalisation method"]
        except:
            print("no norm_method for {}".format(filename))

    ids["fullpath"] = join(relpath(dirpath, release_dir), basename(filename))

    return ids, unit_failures


def _process_dir(helper, database, target_dir, release_dir, cmip_units):
    cmip_unit_failures = []
    fnames = glob(join(target_dir, "**", "*.MAG"), recursive=True)

    executor = ProcessPoolExecutor()
    jobs = [
        executor.submit(_process_file, fname, helper, release_dir, cmip_units)
        for fname in fnames
    ]

    for job in tqdm(as_completed(jobs), total=len(jobs)):
        res, unit_failures = job.result()
        database.append(res)
        if unit_failures is not None:
            cmip_unit_failures.append(unit_failures)

    print("CMIP unit conversion failures:")
    print("- {}".format("\n\n- ".join([v for v in set(cmip_unit_failures)])))

    return len(jobs)


def _choose_grid(options):
    grids = options.grid_labels
    grid_order = ['gn', 'gr', 'gr1', 'gm']

    for best_grid in grid_order:
        if best_grid in grid_order:
            return options[np.array(grids) == best_grid]

    raise ValueError("Unknown grids: {}".format(grids))


def _find_best_file(matches):
    if len(matches) == 1:
        return matches

    version_matches = matches[matches.version == max(matches.version)]
    if len(version_matches) == 1:
        return version_matches

    # Choose the file that spans the greatest time period
    spans = [r.split("-") for r in version_matches.time_range]
    spans = [int(s[1]) - int(s[0]) for s in spans]
    max_span = max(spans)

    span_matches = version_matches[np.array(spans) == max_span]
    if len(span_matches) == 1:
        return span_matches

    grid_matches = _choose_grid(span_matches)
    if len(grid_matches) > 0:
        raise ValueError("More than 1 option left. {}".format(grid_matches))
    return grid_matches


@click.command()
@click.option("--name", "-n", help="Release name", required=True)
@click.option(
    "--target",
    "-t",
    type=click.Choice(["cmip5", "CMIP6"], case_sensitive=False),
    required=True,
)
@click.option(
    "--cmipunits",
    "-c",
    type=click.Path(exists=True, dir_okay=False, resolve_path=True, readable=True),
    default=join(dirname(__file__), "..", "data", "misc", "cmip6_units.csv"),
    show_default=True,
)
@click.option(
    "--dedup/--no-dedup", default=True, show_default=True,
)
@click.argument("paths_of_interest", nargs=-1)
@click.argument("dst", nargs=1)
def create_database(cmipunits, name, target, dedup, paths_of_interest, dst):
    database = []

    release_dir = join(dirname(__file__), "..", "data", target)
    output_dir = join(release_dir, "database")

    if not exists(output_dir):
        os.makedirs(output_dir)

    cmip_units = pd.read_csv(cmipunits)

    helper = get_helper(target)

    for p in paths_of_interest:
        dir_name = join(release_dir, p)
        click.echo("Processing {}".format(dir_name))

        if not exists(dir_name):
            raise click.ClickException("Could not find directory: {}".format(dir_name))

        entries = _process_dir(helper, database, dir_name, release_dir, cmip_units)

        click.echo("found {} entries".format(entries))

    df = pd.DataFrame(database)
    if target == "cmip5":
        df["mip_era"] = "cmip5"
        df = df.rename(
            {
                "activity": "activity_id",
                "model": "source_id",
                "mip_table": "table_id",
                "experiment": "experiment_id",
                "ensemble_member": "member_id",
                "variable_name": "variable_id",
                "time_period": "time_range",
            },
            axis=1,
        )
        df["institution_id"] = ""
        df["grid_label"] = ""
        df["version"] = ""

    if dedup:
        orig_len = len(df)
        columns = [
            "root_dir",
            "mip_era",
            "activity_id",
            "institution_id",
            "source_id",
            "experiment_id",
            "member_id",
            "table_id",
            "variable_id",
            "grid_label",
            "file_ext",
            "timeseriestype",
            "normalisation_method",
        ]
        df = df.fillna("").groupby(columns).apply(_find_best_file)
        print("Found {} duplicated items".format(orig_len - len(df)))

    if df.duplicated().any():
        raise ValueError("Duplicated values in database")

    df.to_csv(join(output_dir, dst), index=False)


if __name__ == "__main__":
    create_database()
