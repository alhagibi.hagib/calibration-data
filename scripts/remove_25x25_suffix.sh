#!/bin/bash
SOURCE_DIR=/data/cmip5_25x25

for i in $(find ${SOURCE_DIR} -name "*.nc" -type f); do 
    echo "$i"
    echo ${i//_25x25.nc/.nc}
    sudo mv -v $i ${i//_25x25.nc/.nc}
done
