#!/bin/env python
"""
Converts the MAG files to CSV for ingestion by the website

creates the cmip5/json and cmip6/json archives
"""
import json
from os.path import isfile, join, dirname, exists
import os
import multiprocessing
import gzip

import click
import numpy as np
import pandas as pd
from pymagicc.io import MAGICCData
from tqdm import tqdm


def ensure_dir_exists(fname):
    d = dirname(fname)
    if not exists(d):
        try:
            os.makedirs(d)
        except FileExistsError:
            # There can be a race condition creating folder
            pass


def process_file(fname):
    if fname.endswith(".json") or fname.endswith(".json.gz"):
        return

    pt2 = fname.replace("MAG", "json.gz").replace("mag", "json_gz")
    if isfile(pt2):
        # already converted
        return

    if isfile(fname) and ".MAG" in fname:
        md = MAGICCData(fname)
        tps = md.time_points.values.astype(np.timedelta64) / np.timedelta64(1, "ms")
        keys = list(md.meta["region"])
        vals = list(md.values)
        js = {}
        for i, key in enumerate(keys):
            js[key] = list(zip(tps, list(vals[i])))
        ensure_dir_exists(pt2)
        with gzip.GzipFile(pt2, 'w') as fout:
            fout.write(json.dumps(js).encode('utf-8'))
    else:
        raise ValueError(
            "How did non-existent, non-MAG file get into database?: {}".format(fname)
        )


@click.command()
@click.option("--name", "-n", help="Release name", required=True)
@click.option(
    "--target",
    "-t",
    type=click.Choice(["cmip5", "CMIP6"], case_sensitive=False),
    required=True,
)
def create_json(name, target):
    release_dir = join(dirname(__file__), "..", "data", target)

    database_file = join(
        release_dir, "database", "database-{}-subset.csv".format(target)
    )
    db = pd.read_csv(database_file)

    with multiprocessing.Pool(16) as pool:
        for _ in tqdm(
            pool.imap(
                process_file,
                [join(release_dir, fpath) for fpath in db["fullpath"]],
                chunksize=10,
            ),
            total=len(db["fullpath"]),
            unit="file",
        ):
            pass
        pool.close()
        pool.join()


if __name__ == "__main__":
    create_json()
