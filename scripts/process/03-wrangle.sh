#!/bin/bash
#
#  Join data files together
#

source ${SCRIPT_DIR}/process/funcs.sh

AVERAGE_START_YEAR_VARIABLES=$(get_config '.average_start_year_variables' true) || exit 1
AVERAGE_MID_YEAR_VARIABLES=$(get_config '.average_mid_year_variables' true) || exit 1
POINT_START_YEAR_VARIABLES=$(get_config '.point_start_year_variables' true) || exit 1

# Only run variables if they are specifed in variables
AVERAGE_START_YEAR_VARIABLES=$(get_overlap $VARIABLE_ID $AVERAGE_START_YEAR_VARIABLES)
AVERAGE_MID_YEAR_VARIABLES=$(get_overlap $VARIABLE_ID $AVERAGE_MID_YEAR_VARIABLES)
POINT_START_YEAR_VARIABLES=$(get_overlap $VARIABLE_ID $POINT_START_YEAR_VARIABLES)

echo "Wrangling AVERAGE_START_YEAR_VARIABLES=$AVERAGE_START_YEAR_VARIABLES AVERAGE_MID_YEAR_VARIABLES=$AVERAGE_MID_YEAR_VARIABLES POINT_START_YEAR_VARIABLES=$POINT_START_YEAR_VARIABLES"


if [ "${TARGET}" = "CMIP6" ]; then
  STITCH_REGEXP="/(${SOURCE_ID//,/|})/(${EXPERIMENT_ID//,/|})/(${VARIANT_LABEL//,/|})/"
else
  STITCH_REGEXP="/(${EXPERIMENT_ID//,/|})/"
fi

echo "Starting to wrangle"

# MAG files point start year (including picontrol)
netcdf-scm wrangle \
  ${CRUNCH_DIR_SUBDIR} \
  "${STITCH_OUTPUT_DIR_ROOT}/mag/point-start-year" \
  "${CONTACT}" \
  --drs "${DRS}" \
  --out-format "mag-files-point-start-year" \
  --number-workers ${STITCH_NUMBER_WORKERS} \
  --regexp "^.*${STITCH_REGEXP}.*/(${POINT_START_YEAR_VARIABLES//,/|})/.*$" $EXTRA_CONFIG

# mag files average year start of year (including picontrol)
netcdf-scm wrangle \
  ${CRUNCH_DIR_SUBDIR} \
  "${STITCH_OUTPUT_DIR_ROOT}/mag/average-year-start-year" \
  "${CONTACT}" \
  --drs "${DRS}" \
  --out-format "mag-files-average-year-start-year" \
  --number-workers ${STITCH_NUMBER_WORKERS} \
  --regexp "^.*${STITCH_REGEXP}.*/(${AVERAGE_START_YEAR_VARIABLES//,/|})/.*$" $EXTRA_CONFIG

# mag files average year middle of year (including picontrol)
netcdf-scm wrangle \
  ${CRUNCH_DIR_SUBDIR} \
  "${STITCH_OUTPUT_DIR_ROOT}/mag/average-year-mid-year" \
  "${CONTACT}" \
  --drs "${DRS}" \
  --number-workers ${STITCH_NUMBER_WORKERS} \
  --out-format "mag-files-average-year-mid-year" \
  --regexp "^.*${STITCH_REGEXP}.*/(${AVERAGE_MID_YEAR_VARIABLES//,/|})/.*$" $EXTRA_CONFIG

echo "Wrangle completed"
