#!/bin/bash
#
#  Crunch the downloaded NetCDF files into region specific data
#

source ${SCRIPT_DIR}/process/funcs.sh

GRID_LABEL=".*"

DOWNLOAD_DIR=$(get_config '.download_dir') || exit 1

CRUNCH_REGIONS=$(get_config ".regions" true) || exit 1
CRUNCH_SMALL_THRESHOLD=900.0
CRUNCH_MEDIUM_THRESHOLD=5000.0
FORCE_LAZY_THRESHOLD=1000000.0

CRUNCH_REGEXP=".*"

# for CMIP5 crunch everything
if [ "${TARGET}" = "CMIP6" ]; then
  if [ "${SOURCE_ID}" != ".*" ]; then

    echo "Adding source id to crunch regexp"
    CRUNCH_REGEXP="${CRUNCH_REGEXP}/(${SOURCE_ID//,/|})"

  fi

  CRUNCH_REGEXP="${CRUNCH_REGEXP}/(piControl|${EXPERIMENT_ID//,/|})/"

  if [ "${VARIANT_LABEL}" != ".*" ]; then

    echo "Adding variant label to crunch regexp"
    CRUNCH_REGEXP="${CRUNCH_REGEXP%/}/(${VARIANT_LABEL//,/|})/"

  fi

  CRUNCH_REGEXP="${CRUNCH_REGEXP}.*mon.*"

  if [ "${VARIABLE_ID}" != ".*" ]; then

    echo "Adding variable id to crunch regexp"
    CRUNCH_REGEXP="${CRUNCH_REGEXP%/}/(${VARIABLE_ID//,/|})/"
  fi

  if [ "${GRID_LABEL}" != ".*" ]; then

    echo "Adding grid label to crunch regexp"
    CRUNCH_REGEXP="${CRUNCH_REGEXP%/}/(${GRID_LABEL//,/|})/"

  fi

  CRUNCH_REGEXP="${CRUNCH_REGEXP}.*"
fi

echo "Starting to crunch"

# crunch downloaded data
netcdf-scm crunch \
  ${DOWNLOAD_DIR} \
  ${CRUNCH_DIR} \
  "${CONTACT}" \
  --drs "${DRS}" \
  --regions "${CRUNCH_REGIONS}" \
  --small-number-workers ${CRUNCH_SMALL_NUMBER_WORKERS} \
  --small-threshold ${CRUNCH_SMALL_THRESHOLD} \
  --medium-number-workers ${CRUNCH_MEDIUM_NUMBER_WORKERS} \
  --medium-threshold ${CRUNCH_MEDIUM_THRESHOLD} \
  --force-lazy-threshold ${FORCE_LAZY_THRESHOLD} \
  --regexp "${CRUNCH_REGEXP}"

echo "Crunch completed"
