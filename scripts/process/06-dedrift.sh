#!/bin/bash
#
#  Dedrifting 
#

source ${SCRIPT_DIR}/process/funcs.sh

DEDRIFT_VARIABLES=$(get_config '.dedrift_variables' true) || exit 1
DEDRIFT_VARIABLES=$(get_overlap $VARIABLE_ID $DEDRIFT_VARIABLES)

echo "Dedrifting $DEDRIFT_VARIABLES"

STITCH_NORM_METHOD="21-yr-running-mean-dedrift"
if [ "${TARGET}" = "CMIP6" ]; then
  STITCH_REGEXP="/(${SOURCE_ID//,/|})/(${EXPERIMENT_ID//,/|})/(${VARIANT_LABEL//,/|})/"
else
  STITCH_REGEXP="/(${EXPERIMENT_ID//,/|})/"
fi

EXTRA_CONFIG=$(get_stitch_config)


echo "Starting to dedrift"

# point start of year dedrifted
netcdf-scm stitch \
  ${CRUNCH_DIR_SUBDIR} \
  "${STITCH_OUTPUT_DIR_ROOT}/mag/point-start-year" \
  "${CONTACT}" \
  --drs "${DRS}" \
  --out-format "mag-files-point-start-year" \
  --number-workers ${STITCH_NUMBER_WORKERS} \
  --regexp "^(?!.*(piControl)).*${STITCH_REGEXP}.*/(${DEDRIFT_VARIABLES//,/|})/.*$" \
  --normalise ${STITCH_NORM_METHOD} \
  --prefix "DEDRIFTED" $EXTRA_CONFIG


# mag files average year middle of year dedrifted
netcdf-scm stitch \
  ${CRUNCH_DIR_SUBDIR} \
  "${STITCH_OUTPUT_DIR_ROOT}/mag/average-year-mid-year" \
  "${CONTACT}" \
  --drs "${DRS}" \
  --out-format "mag-files-average-year-mid-year" \
  --number-workers ${STITCH_NUMBER_WORKERS} \
  --regexp "^(?!.*(piControl)).*${STITCH_REGEXP}.*/(${DEDRIFT_VARIABLES//,/|})/.*$" \
  --normalise ${STITCH_NORM_METHOD} \
  --prefix "DEDRIFTED" $EXTRA_CONFIG


echo "dedrift complete"
