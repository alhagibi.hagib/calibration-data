from collections import defaultdict
from glob import glob
from os.path import join

import pandas as pd
import tqdm.autonotebook as tqdm
from netcdf_scm.citing import get_citation_tables
from netcdf_scm.io import load_mag_file, pymagicc
from scmdata import ScmRun

# make sure tables don't get cut off
pd.set_option("display.max_colwidth", None)

OUT_DIR = "references"


def short_read(fp):
    out = ScmRun(
        data=[1],
        index=[1],
        columns={
            "model": "not set",
            "scenario": "not set",
            "variable": "not set",
            "unit": "not set",
            "region": "not set",
        },
    )
    out.metadata = pymagicc.io.read_mag_file_metadata(fp)

    return out


pymagicc.io.MAGICCData = short_read

files_to_check = glob(
    join("data", "cmip5", "ascii", "mag", "monthly", "**", "*.MAG"), recursive=True
)
db_cmip5 = [
    load_mag_file(f, drs="MarbleCMIP5")
    for f in tqdm.tqdm(files_to_check, desc="Loading files")
]

files_to_check = glob(
    join("data", "CMIP6", "ascii", "mag", "monthly", "**", "*.MAG"), recursive=True
)
db_cmip6 = [
    load_mag_file(f, drs="CMIP6Output")
    for f in tqdm.tqdm(files_to_check, desc="Loading files")
]

db = db_cmip5 + db_cmip6

db_sorted = defaultdict(list)
for scmrun in db:
    db_sorted[scmrun.get_unique_meta("climate_model", no_duplicates=True)].append(
        scmrun
    )

citation_table_bits = {
    "cmip5_bibtex": [],
    "cmip6_bibtex": [],
    "cmip5_references": [],
    "cmip6_references": [],
}
for model, db_model in tqdm.tqdm(db_sorted.items(), desc="Model citations"):
    try:
        model_table_bits = get_citation_tables(db_model)
        for k, v in model_table_bits.items():
            if isinstance(v, list):
                citation_table_bits[k] += v
            else:
                citation_table_bits[k].append(v)

    except Exception as e:
        print("Exception for {}: {}".format(model, e))

citation_table_bits["cmip6_references"] = pd.concat(
    citation_table_bits["cmip6_references"]
)
citation_table_bits["cmip5_references"] = pd.concat(
    citation_table_bits["cmip5_references"]
)

with open(join(OUT_DIR, "cmip_refs.bib"), "w") as fh:
    fh.write("\n\n".join(citation_table_bits["cmip5_bibtex"]))
    fh.write("\n\n".join(citation_table_bits["cmip6_bibtex"]))

citation_table_bits["cmip6_references"].to_latex(
    join(OUT_DIR, "cmip6_references_table.tex")
)
citation_table_bits["cmip5_references"].to_latex(
    join(OUT_DIR, "cmip5_references_table.tex")
)
citation_table_bits["cmip6_references"].to_csv(
    join(OUT_DIR, "cmip6_references_table.csv")
)
citation_table_bits["cmip5_references"].to_csv(
    join(OUT_DIR, "cmip5_references_table.csv")
)

# retraction_table = check_depends_on_retracted(files_to_check, esgf_query_batch_size=100, nworkers=8)
# retraction_table.to_csv("retraction_table.csv", index=False)


# retracted_files = retraction_table.loc[retraction_table["dependency_retracted"], :]

# retracted_mag_files = retracted_files["mag_file"].unique()
# print("{} MAG files based on retracted data".format(retracted_mag_files.shape[0]))
# pprint(retracted_mag_files.tolist())
# print()

# retracted_nc_files = retracted_files["dependency_file"].unique()
# print("{} retracted nc files".format(retracted_nc_files.shape[0]))
# pprint(retracted_nc_files.tolist())
