import os

import pandas as pd

retraction_table = pd.read_csv("retraction_table.csv")
retracted_files = retraction_table.loc[retraction_table["dependency_retracted"], :]

for f in retracted_files["mag_file"].unique():
    try:
        os.remove(f)
    except Exception as e:
        print(f, e)
