#!/bin/bash
#
# Process CMIP6 data
#

if [ $# -eq 0 ]
  then
    echo "No arguments supplied. Pass config filename"
    exit
fi

echo "Reading configuration from $1"
export CONFIG=$(cat "$1")

# Load helper funcs
export SCRIPT_DIR=$(dirname "$BASH_SOURCE")
source ${SCRIPT_DIR}/process/funcs.sh


# Fixed configuration
export ROOT="/data/calibration-data"


TARGET=$(get_config '.target') || exit 1
DRS=$(get_config '.drs') || exit 1
SOURCE_ID=$(get_config '.source_id' true) || exit 1
VARIANT_LABEL=$(get_config '.variant_label' true) || exit 1
VARIABLE_ID=$(get_config '.variable_id' true) ||exit 1
EXPERIMENT_ID=$(get_config '.experiment_id' true) || exit 1
CRUNCH_DIR=$(get_config '.crunch_dir') || exit 1
STITCH_OUTPUT_DIR_ROOT=$(get_config '.stitch_dir') || exit 1

# Steps
DOWNLOAD=$(get_config '.steps.download') || exit 1
CRUNCH=$(get_config '.steps.crunch') || exit 1
WRANGLE=$(get_config '.steps.wrangle') || exit 1
STITCH=$(get_config '.steps.stitch') || exit 1
NORM=$(get_config '.steps.norm') || exit 1
DEDRIFT=$(get_config '.steps.dedrift') || exit 1


export CRUNCH_DIR
export CRUNCH_DIR_SUBDIR="${CRUNCH_DIR}/netcdf-scm-crunched/${TARGET}"
export STITCH_OUTPUT_DIR_ROOT
export CONTACT="Zebedee Nicholls <zebedee.nicholls@climate-energy-college.org>, Jared Lewis <jared.lewis@climate-energy-college.org>, Malte Meinshausen <malte.meinshausen@unimelb.edu.au>"
export STITCH_NUMBER_WORKERS=50
export CRUNCH_MEDIUM_NUMBER_WORKERS=25
export CRUNCH_SMALL_NUMBER_WORKERS=50

# Export variables used in the processing scripts
export TARGET
export DRS
export SOURCE_ID
export VARIANT_LABEL
export VARIABLE_ID
export EXPERIMENT_ID


if [ "${DOWNLOAD}" = true ]; then
  bash ${SCRIPT_DIR}/process/01-download.sh || exit 1
fi
if [ "${CRUNCH}" = true ]; then
  bash ${SCRIPT_DIR}/process/02-crunch.sh || exit 1
fi
if [ "${WRANGLE}" = true ]; then
  bash ${SCRIPT_DIR}/process/03-wrangle.sh || exit 1
fi
if [ "${STITCH}" = true ]; then
  bash ${SCRIPT_DIR}/process/04-stitch.sh || exit 1
fi
if [ "${NORM}" = true ]; then
  bash ${SCRIPT_DIR}/process/05-norm.sh || exit 1
fi
if [ "${DEDRIFT}" = true ]; then
  bash ${SCRIPT_DIR}/process/06-dedrift.sh || exit 1
fi

echo
echo "all jobs completed"
