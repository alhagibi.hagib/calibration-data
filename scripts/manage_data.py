#!/bin/env python
"""
Manage the process of creating releases of the CMIP6 calibration data.

The purpose of this to create a repeatable releases of the latest CMIP6 data.
This tool handles the management of the S3 storage bucket for the data, see
the `run-process.sh` script for a master script which creates the data.


## Getting Started

This requires valid AWS credentials for the MAGICC AWS account. Ask
Jared to create these for you.

## Release Lifecycle

First step is to create a new release using the `release` command. This requires
a unique release name. This release name will be used to reference the release
throughout the release process.

```
manage_data.py release --name 20200417
```

If that command was successful, metadata about the release was added to an
S3 bucket. After that has happpened data folders can be uploaded.

Multiple datasets can be uploaded to a given release up until the
the release is finalised. Once finalised not new data can be added to the
release. The `add` command can be called once new datasets have been generated.

```
manage_data.py add --name 20200417 netcdf
./create-ascii-files.sh
manage_data.py add --name 20200417 ascii
```

The `set_latest` command finalised the release and marks the release as the
current version of the calibration data.

`manage_data.py set_latest --name 20200417`

"""

import fnmatch
import json
import multiprocessing
import re
import socket
import subprocess
from datetime import datetime
from getpass import getuser
from glob import glob
from io import BytesIO
from os import makedirs
from os.path import basename, dirname, join, relpath, exists, isfile

import boto3
import click
from botocore.exceptions import ClientError
from tqdm import tqdm

# cmip6 user only has RW access to the cmip6-calibration-data bucket
s3 = boto3.client("s3", region_name="ap-southeast-2")

S3_BUCKET = "cmip6-calibration-data"

username = getuser()
hostname = socket.gethostname()
user_at_host = "{}@{}".format(username, hostname)


def key_exists(key, bucket=S3_BUCKET):
    try:
        s3.head_object(Bucket=bucket, Key=key)
        return True
    except ClientError as e:
        if e.response["Error"]["Code"] == "404":
            return False
        else:
            # Something else has gone wrong.
            raise


def upload_json(key, data, bucket=S3_BUCKET):
    data["updated_at"] = datetime.utcnow().isoformat()
    data["updated_by"] = user_at_host
    data_str = json.dumps(data)
    buff = BytesIO(data_str.encode())
    s3.upload_fileobj(Fileobj=buff, Bucket=bucket, Key=key)


def upload_file(p):
    fname, key = p
    s3.upload_file(Bucket=S3_BUCKET, Key=key, Filename=fname)


def read_json(key, bucket=S3_BUCKET):
    try:
        res = s3.get_object(Bucket=bucket, Key=key)
    except ClientError:
        raise click.ClickException("Valid release not found")
    return json.load(res["Body"])


class State(object):
    def __init__(self):
        self.target = ""
        self.name = ""

    def get_key(self, key):
        assert self.name
        return "{}/{}/{}".format(self.target, self.name, key)

    def get_release_data(self):
        return read_json(self.get_key("release.json"))

    @property
    def releases_key(self):
        return "{}/releases.json".format(self.target)


pass_state = click.make_pass_decorator(State, ensure=True)


def target_option(f):
    def callback(ctx, param, value):
        state = ctx.ensure_object(State)
        state.target = value
        return value

    return click.option(
        "--target",
        "-t",
        help="Which target to create the release for",
        default="CMIP6",
        type=click.Choice(["cmip5", "CMIP6"], case_sensitive=False),
        callback=callback,
    )(f)


def name_option(f, required=True):
    def callback(ctx, param, value):
        state = ctx.ensure_object(State)
        state.name = value
        return value

    return click.option(
        "--name",
        "-n",
        help="Revision name. Typically the date is used in the form YYYYMMDD",
        required=required,
        callback=callback,
    )(f)


def common_options(name_required=True):
    def _wrap(f):
        f = target_option(f)
        f = name_option(f, name_required)
        return f

    return _wrap


@click.group(help="Management tool for releasing CMIP6 data")
def cli():
    pass


@cli.command(help="Create a new release")
@pass_state
@common_options()
def release(state, name, **kwargs):
    # Check that the release doesn't exist
    if key_exists(state.get_key("release.json")):
        raise click.ClickException(
            "Release already exists. Choose a different release name"
        )

    # Upload release.json
    upload_json(
        state.get_key("release.json"),
        {
            "name": name,
            "created_at": datetime.utcnow().isoformat(),
            "created_by": user_at_host,
            "hostname": hostname,
            "username": username,
            "hash": subprocess.check_output(["git", "describe", "--always", "--dirty"])
            .strip()
            .decode("ascii"),
            "is_finalised": False,
            "uploads": [],  # Will hold metadata about each upload
        },
    )

    if key_exists(state.releases_key):
        release_data = read_json(state.releases_key)
    else:
        release_data = {"releases": [], "latest": "NONE"}

    release_data["releases"].append({"name": name, "path": name, "is_finalised": False})
    upload_json(state.releases_key, release_data)

    click.echo("New release '{}' created".format(name))


@cli.command(help="Add a dataset to a release")
@click.option(
    "--force",
    is_flag=True,
    help="Forcibly copy files ignoring existing files or state of release",
)
@click.option(
    "--prefix",
    "-p",
    help="Prefix to place files. Must not exist. The name of the dir is used if no prefix is provided",
)
@click.argument("dir", type=click.Path(), required=True)
@pass_state
@common_options()
def add(state, name, dir, force, prefix, **kwargs):
    if not key_exists(state.get_key("release.json")):
        raise click.ClickException(
            "Release doesn't exist. Create with `release` command first"
        )

    release_data = state.get_release_data()

    # Check is_finalised field
    if release_data["is_finalised"]:
        if force:
            click.echo("Warning: release is already finalised, but force=True")
        else:
            raise click.ClickException("Release is already finalised")

    # Check if prefix has already been uploaded
    if prefix is None:
        prefix = basename(dir)
    prefix = prefix.strip("/")

    for u in release_data["uploads"]:
        if u["prefix"].startswith(prefix):
            if force:
                click.echo(
                    "Warning: prefix {} already exists, but force=True".format(
                        u["prefix"]
                    )
                )
            else:
                raise click.ClickException(
                    "Existing prefix {} clashes. Try using a different prefix".format(
                        u["prefix"]
                    )
                )

    files_to_upload = [f for f in glob(join(dir, "**/*"), recursive=True) if isfile(f) and not f.endswith(".log")]
    if not len(files_to_upload):
        raise click.ClickException("No files found")
    click.echo("{} files to upload to prefix {}".format(len(files_to_upload), prefix))

    # Check if any files exist with the prefix
    res = s3.list_objects_v2(Bucket=S3_BUCKET, Prefix=state.get_key(prefix), MaxKeys=1)

    if res["KeyCount"]:
        if force:
            click.echo(
                "Warning: Files with prefix {} in release {} already exist, but force=True overwriting".format(
                    prefix, name
                )
            )
        else:
            raise click.ClickException(
                "Files with prefix {} in release {} already exist".format(prefix, name)
            )

    with multiprocessing.Pool(32) as pool:
        for _ in tqdm(
            pool.imap(
                upload_file,
                [
                    (f, state.get_key(join(prefix, relpath(f, dir))))
                    for f in files_to_upload
                ],
            ),
            total=len(files_to_upload),
            unit="file",
        ):
            pass
        pool.close()
        pool.join()

    release_data["uploads"].append(
        {
            "prefix": prefix,
            "count": len(files_to_upload),
            "created_at": datetime.utcnow().isoformat(),
            "created_by": user_at_host,
        }
    )
    upload_json(state.get_key("release.json"), release_data)

    click.echo("Files uploaded successfully")


@cli.command()
@click.option("--exclude", multiple=True, help="Exclude files from download. i.e. *.nc")
@click.option(
    "--force", is_flag=True, help="Forcibly download files ignoring existing files"
)
@click.argument("src", nargs=-1)
@click.argument("dest", type=click.Path(exists=True), nargs=1)
@pass_state
@common_options(name_required=False)
def fetch(state, exclude, force, src, dest, **kwargs):
    """
    Fetch datasets from a release.

    Downloaded files maintain their heirachy. For example the files from dataset `netcdf` from the
    release `release`would be stored as under `out/release/netcdf when dest is `out`
    """
    if state.name is None:
        release_data = read_json(state.releases_key)
        state.name = release_data["latest"]

    release_data = state.get_release_data()

    def check_if_download(f):
        for e in exclude:
            if re.match(fnmatch.translate(e), f["Key"]):
                return False
        if exists(join(dest, f["Key"])) and not force:
            return False
        return True

    def download_dataset(prefix):
        paginator = s3.get_paginator("list_objects_v2")
        files_to_download = []
        for resp in paginator.paginate(Bucket=S3_BUCKET, Prefix=state.get_key(prefix)):
            for f in resp.get("Contents", []):
                if check_if_download(f):
                    files_to_download.append(f["Key"])

        if len(files_to_download):
            click.echo("Fetching {} files".format(len(files_to_download)))
            for f in tqdm(files_to_download, unit="file"):
                fname = join(dest, f)
                if not exists(dirname(fname)):
                    makedirs(dirname(fname))
                s3.download_file(Bucket=S3_BUCKET, Key=f, Filename=fname)

    for upload in release_data["uploads"]:
        prefix = upload["prefix"]
        if prefix in src or len(src) == 0:
            click.echo(
                "Fetching dataset '{}' for release {}".format(prefix, state.name)
            )
            download_dataset(prefix)

    click.echo("Fetch complete")


@cli.command(
    help="Finalises a release so no new data can be added and sets the release as the latest release"
)
@pass_state
@common_options()
def set_latest(state, name, **kwargs):
    # Check that the release doesn't exist
    if not key_exists(state.get_key("release.json")):
        raise click.ClickException(
            "Release doesn't exist. Create with `release` command first"
        )

    release_data = state.get_release_data()
    release_data["is_finalised"] = True
    upload_json(state.get_key("release.json"), release_data)

    release_data = read_json(state.releases_key)
    release_found = False
    for r in release_data["releases"]:
        if r["name"] == name:
            r["is_finalised"] = True
            release_found = True

    if not release_found:
        raise click.ClickException("Could not find release info in releases.json")

    release_data["latest"] = name
    upload_json(state.releases_key, release_data)

    click.echo("'{}' set as the latest release".format(name))


@cli.command(help="Get the latest release")
@pass_state
@target_option
def latest(state, **kwargs):
    release_data = read_json(state.releases_key)

    click.echo(release_data["latest"])


if __name__ == "__main__":
    cli()
