#!/bin/bash
find ./crunched/netcdf-scm-crunched/CMIP6/ -mindepth 9 -type d -exec bash -c "echo -ne '{} '; ls '{}' | wc -l" \; | awk '$NF>1'

